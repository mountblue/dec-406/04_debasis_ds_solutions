class Node {
    constructor(data, left = null , right = null){
        this.data = data
        this.left = left
        this.right = right
    }
}

class BinaryTree {
    constructor(){
        this.root = null
    }
    
    search(data){
        return this.searchRecursively(this.root, data)
    }
    height(node){
        if(node === null) {
            return 0;
        }
        let leftHeight = this.height(node.left)
        let rightHeight = this.height(node.right)
        let h = 0;
        if(leftHeight > rightHeight){
            h = ++leftHeight
        }else {
            h = ++rightHeight
        }
        return h
    }

    insert(data){
        const node = new Node(data)
        if(this.root === null) {
            this.root = node
            return;
        }else {
            return this.insertRecursively(this.root,data)            
        }
    }
    insertRecursively(node,data){
        if(data < node.data){
            if(node.left === null) {
                node.left = new Node(data)
                return;
            }else if ( node.left !== null ){
                return this.insertRecursively(node.left, data)
            }
        }else if(data > node.data) {
            if(node.right === null ){
                node.right = new Node(data)
                return;
            }else if( node.right !== null ) {
                return this.insertRecursively(node.right, data)
            } 
        }else {
            return null
        }
    }
    
    searchRecursively(node, findValue) {
        if(node) {
            if(node.data === findValue) {
                return true
            } else {
                return this.searchRecursively(node.left, findValue) ||
                this.searchRecursively(node.right, findValue)
            }
            
        } else {
            return false
        }
    }

    levelOrder(){
        let container = []
        if(this.root === null) {
            return;
        }
        let queue = []
        queue.push(this.root,null)
        while(queue.length > 0 && queue[0] !== null) {
            container.push(queue[0].data)
            let node = queue.shift()
            if(node.left !== null) {
                queue.push(node.left)
            }
            if(node.right !== null){
                queue.push(node.right)
            }
            if(queue[0] === null ){
                queue.shift()
                container.push(" ")
                queue.push(null)
            }
        }
        return container
    }
}

const data  = {
    Node, BinaryTree
}

module.exports = data;