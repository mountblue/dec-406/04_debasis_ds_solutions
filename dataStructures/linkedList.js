class Node {
    constructor(data){
        this.data = data
        this.next = null
    }
}

class LinkedList{
    constructor(){
        this.head = null
        this.length = 0
    }
    print(){
        let represent = "";
        if(this.head === null){
            represent = "This list is empty"
        }else{
            let curr = this.head
            represent += `${curr.data}-`
            while(curr.next){
                curr = curr.next
                represent += `${curr.data}-`
            }
            represent += `tail`
        }
        return represent
    }
    add(data){
       let curentData = new Node(data);
       if(this.head === null){
           this.head = curentData
       }else{
           let curr = this.head
           while(curr.next){
               curr = curr.next
           }
           curr.next = curentData
       } 
       this.length++
    }
    
}
data = {
    LinkedList, Node
}

module.exports = data;