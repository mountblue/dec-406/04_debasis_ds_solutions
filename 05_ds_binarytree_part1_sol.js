const data = require("./dataStructures/binaryTree");
const { BinaryTree, Node } = data;

let tree =  new BinaryTree()
tree.insert(4)
tree.insert(2)
tree.insert(9)
tree.insert(3)
tree.insert(5)
tree.insert(7)

let tree1 = new BinaryTree()
tree1.root = new Node(1)
tree1.root.left = new Node(2)
tree1.root.right = new Node(3)
tree1.root.left.left = new Node(4)
tree1.root.left.right = new Node(5)
tree1.root.right.right = new Node(7)
tree1.root.left.left.left = new Node(8); 
tree1.root.right.left = new Node(9); 
tree1.root.right.left = new Node(10);
console.log(tree1)
/**
 * Given a binary tree of size N+1, your task is to complete the function tiltTree(), that return the tilt of the whole tree. The tilt of a tree node is defined as the absolute difference between the sum of all left subtree node values and the sum of all right subtree node values. Null nodes are assigned tilt to be zero. Therefore, tilt of the whole tree is defined as the sum of all nodes’ tilt.
Examples:
Input :
1
/ \
2 3
Output : 1
Explanation:
Tilt of node 2 : 0
Tilt of node 3 : 0
Tilt of node 1 : |2-3| = 1
Tilt of binary tree : 0 + 0 + 1 = 1
Input :
	 4
   /  \
  2   9
 / \   \
3  5   7
Output : 15
Explanation:
Tilt of node 3 : 0
Tilt of node 5 : 0
Tilt of node 7 : 0
Tilt of node 2 : |3-5| = 2
Tilt of node 9 : |0-7| = 7
Tilt of node 4 : |(3+5+2)-(9+7)| = 6
Tilt of binary tree : 0 + 0 + 0 + 2 + 7 + 6 = 15
 
 * @param {*} node 
 * @param {*} sum 
 */


function calculateTilt(node,sum){
	if(node === null) {
		return 0
	}
	let data =  parseInt(node.data)
	let sumLeft = calculateTilt(node.left, sum)
	let sumRight = calculateTilt(node.right, sum)
	sum += Math.abs(sumLeft - sumRight)
	return data + sumLeft + sumRight
}
console.log(calculateTilt(tree1.root, 0))

// ===================================
/**
 * 
 *Given a Binary Tree you need to find maximum value which you can get by subtracting value of node B from value of node A, where A and B are two nodes of the binary tree and A is an ancestor of B . You are required to complete the function maxDiff . You should not read any input from stdin/console. There are multiple test cases. For each test case, this method will be called individually.
Input:
The task is to complete the function maxDiff which takes 1 argument, root of the Tree . The struct node has a data part which stores the data, pointer to left child and pointer to right child.
There are multiple test cases. For each test case, this method will be called individually.
Output:
The function should return an integer denoting the maximum difference.
Constraints:
1 <=T<= 30
1 <=Number of nodes<= 100
1 <=Data of a node<= 1000
Example
Input
1
2
5 2 L 5 1 R
Output
4
         5
		/  \
		2   1
In above example there is one test case which represents a tree with 3 nodes and 2 edges where root is 5, left child of 5 is 2 and right child of 5 is 1 hence the max difference we can get is from 5 and 1 ie 4 . 
 */
let containerOfMaxDiff = []
function maxDiff(root){
	if(root === null){
		return;
	}
	if(root.left === null){
		if(root.right === null){
			return;
		}else{
			containerOfMaxDiff.push(Math.abs(root.data - root.right.data))
		}
		return;
	}else {
		containerOfMaxDiff.push(Math.abs(root.data - root.left.data))
	}
	maxDiff(root.left)
	maxDiff(root.right)
	return Math.max(...containerOfMaxDiff)
}
// console.log(maxDiff(tree.root))

/**
 * 2. Given a Complete Binary tree, print the level order traversal in sorted order.
Input:
The first line of the input contains integer T denoting the number of test cases. For each test case, the first line takes an integer n denoting the size of array i.e number of nodes followed by n-space separated integers denoting the nodes of the tree in level order fashion.
Output:
For each test case, the output is the level order sorted tree.
Note: For every level, we only print distinct elements.
Constraints:
1<=T<=100
1<=n<=10^5
Example:
Input:
2
7
7 6 5 4 3 2 1findRightSibling(tree.root,1)
6
5 6 4 9 2 1
Output:
7
5 6
1 2 3 4
5
4 6
1 2 9
Explanation:
Tree looks like this
		  7
		/  \
	  6    5
	/ \   / \
   4  3  2   1
Sorted order:
7
5 6
1 2 3 4
 */

const getAllElements = new Set()
function traverseTheTree(root){
	if(root === null) {
		return;
	}
	getAllElements.add(root.data)
	if(root.left == null || root.right == null) {
		getAllElements.add(root.data)
	}
	traverseTheTree(root.left)
	traverseTheTree(root.right)
	return printInSortedWay([...getAllElements])
}
function printInSortedWay(array){
	let getOrderedValue = []
	let count = 0;	
	while(array.length) {
		getOrderedValue.push(array.splice(0,2**count))
		count++
	}
	return getOrderedValue.map((el,i)=> {
		if((i+1) % 2 === 1){
			return el.reverse()
		}else{
			return el
		}
	})
}
// console.log(traverseTheTree(tree.root))

/**
 * Given a binary tree, your task is to complete the function findRightSibling(), that should return the right sibling to a given node if it doesn’t exist return null.
		 1
		/ \
	   2   3
	  / \  \
	 4  6   5
	/   \   \
	7   9   8
   / \
  10  12
Input : Given above tree with parent pointer and node 10
Output : 12
 */

function findRightSibling( tree, find_val ) {
	const levels = String(tree.levelOrder()).trim()
	const nodesInlevel =  levels.split(" ").map(el => el.split(",").filter(v => v !== "").map(Number))
	let indexFound = nodesInlevel.filter(el => el.includes(find_val))[0]
	const value = indexFound[indexFound.indexOf(find_val)+1]
	return value === undefined ? null: value;
}

// console.log(findRightSibling(tree,8))


