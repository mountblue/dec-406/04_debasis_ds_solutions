/**
 * Delete the middle element of the stack. Given a stack with push(), pop(), empty() operations, delete middle of it without using any additional data structure.
 */

function removeMiddle(stack,size,count){
    if(stack.empty() || size === count){
        return;
    }
    let x = stack.pop()
    removeMiddle(stack,size,count+1)
    if(count !== Math.floor( size / 2 )){
        stack.push(x)
    }

}

// let stack = [1,2,3,4,5]
// removeMiddle(stack,5,0)
// console.log(stack)

/**
 * Print Bracket Number. Given an elression el of length n consisting of some brackets. The task is to print the bracket numbers when the elression is being parsed.
Examples :
Input : (a+(b*c))+(d/e)
Output : 1 2 2 1 3 3
The highlighted brackets in the given elression
(a+(b*c))+(d/e) has been assigned the numbers as:
1 2 2 1 3 3.
Input : ((())(()))
Output : 1 2 3 3 2 4 5 5 4 1
 */


function printBracketNumber(exp) { 
    let leftCounter = 1, stack = [],counter = 0
    let main = []
    for (let el of exp) {  
        if (el === '(') { 
            main.push(leftCounter) 
            stack[counter++] = leftCounter;
            leftCounter++
        } else if(el === ')') { 
            main.push(stack[counter-1]);            
            stack[counter - 1] = 1; 
            counter-- 
        } 
    } 
    return main
} 

// console.log(printBracketNumber("((())(()))"))

/**
 * Given array A[] of integers, the task is to complete the function findMaxDiff which finds the maximum absolute difference between nearest left and right smaller element of every element in array.If the element is the leftmost element, nearest smaller element on left side is considered as 0. Similarly if the element is the rightmost elements, smaller element on right side is considered as 0.
 */

 function maxDifference(arr){
     let allDiffs = []
     for(let i = 0; i < arr.length ; i++){
         let leftMost = arr[i-1] === undefined ? 0 : arr[i-1];
         let rightMost = arr[i+1] === undefined ? 0 : arr[i+1];
         let absDiff = Math.abs(leftMost - rightMost)
         allDiffs.push(absDiff) 
     }
     return Math.max(...allDiffs)
 }
//  console.log(maxDifference([1,2,3,4,5,6]))

/**
 * Given an array A of size N having distinct elements, the task is to find the next greater element for each element of the array in order of their appearance in the array. If no such element exists, output -1.
 */
function findNextGreater(arr){
    let greaterNumbers = []
    arr.forEach((el,i,arr) => {
        let nextVals = arr.slice(i+1)
        let foundValue = nextVals.find(val => val > el );
        if(foundValue === undefined){
            greaterNumbers.push(-1)
        }else{
            greaterNumbers.push(foundValue)
        }
    })
    return greaterNumbers;
}

// console.log(findNextGreater([1,11,2,23,44,6,9]))

/**
 * Given an array of integers, find the nearest smaller number for every element such that the smaller element is on left side.If no small element present on the left print -1.
 */

 function findNearestSmallInteger(arr){
    let smallerNumbers = []
    arr.forEach((el,i,arr) => {
        let nextVals = arr.slice(0,i)
        let foundValue = nextVals.reverse().find(val => val < el );
        if(foundValue === undefined){
            smallerNumbers.push(-1)
        }else{
            smallerNumbers.push(foundValue)
        }
    })
    return smallerNumbers;
 }

 console.log(findNearestSmallInteger([1,11,2,23,44,6,9]))