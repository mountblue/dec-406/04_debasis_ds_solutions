const data = require("./datasstructures/linkedList");
const { LinkedList, Node } = data;

let ll = new LinkedList()
ll.add(1)
ll.add(1)
ll.add(3)
ll.add(5)
ll.add(5)
ll.add(8)
ll.add(8)
ll.add(9)
ll.add(10)

/**
 * Given a Linked List of integers, write a function to modify the linked list such that all even numbers appear before all the odd numbers in the modified linked list. Also, keep the order of even and odd numbers same.
Input:
The first line of input contains an integer T denoting the number of test cases.
The first line of each test case is N,N is the number of elements in Linked List.
The second line of each test case contains N input,elements in Linked List.
Output:
Print the all even numbers then odd numbers in the modified Linked List.
Constraints:
1 ≤ T ≤ 100
1 ≤ N ≤ 100
1 ≤ size of elements ≤ 1000
Example:
Input
3
7
17 15 8 9 2 4 6
4
1 3 5 7
7
8 12 10 5 4 1 6
Output
8 2 4 6 17 15 9
1 3 5 7
8 12 10 4 6 5 1
 * @param {*} list 
 */


function sortALinkedList(list){
    let modifiedList = new LinkedList()
    let evens = []
    let odds = []
    if(list.head === null){
        return list.print()
    }else{
        let curr = list.head;
        while(list.length){
            if(curr.data % 2 === 0){
                evens.push(curr.data)
            }else{
                odds.push(curr.data)
            }
            curr = curr.next
            list.length--
        }
    }
    [...evens,...odds].forEach(el => modifiedList.add(el))
    return modifiedList
}



// console.log(ll.print())
// let newlist = sortALinkedList(ll)
// console.log(newlist.print())

/**
 * 2. Write a Count() function that counts the number of times a given int occurs in a list. The code for this has the classic list traversal structure as demonstrated in Length().
 * @param {*} list 
 * @param {*} value 
 */
function countTheOccurance(list,value){
    let count = 0;
    let curr = list.head;
    if(curr === null){
        return list.print()
    }else{
        while(curr.next){
            curr = curr.next
            if(curr.data === value) {
                count++
            }
        }
    }
    return count;
}

// console.log(countTheOccurance(ll,4))

/**
 * Write a Pop() function that is the inverse of Push(). Pop() takes a non-empty list, deletes the head node, and returns the head node's data. If all you ever used were Push() and Pop(), then our linked list would really look like a stack. However, we provide more general functions like GetNth() which what make our linked list more than just a stack. Pop() should assert() fail if there is not a node to pop.
 */

function pop(list){
    if(list.head === null){
        return list.print()
    }else{
        let curr =  list.head;
        list.head = curr.next;
        return curr.data
    }
}
// console.log(pop(ll))

/**
 *  Write a SortedInsert() function which given a list that is sorted in increasing order, and a single node, inserts the node into the correct sorted position in the list. While Push() allocates a new node to add to the list, SortedInsert() takes an existing node, and just rearranges pointers to insert it into the list.
 */

function sortedInsert(list, data){
    let curr = list.head
    let node = new Node(data)
    if(curr === null){
        return list.print()
    }else{
        while(curr.next){
            prev = curr
            curr = curr.next
            if(curr.data > data){
                prev.next = node;
                node.next = curr
                break
            }
        }
        if(curr.next === null){
            curr.next = node
        }
    }
    return list.print()
}
// console.log(sortedInsert(ll,11))

/**
 * Write a RemoveDuplicates() function which takes a list sorted in increasing order and deletes any duplicate nodes from the list. Ideally, the list should only be traversed once.
 */

 function removeDuplicates(list){
     let curr = list.head;
     let values = new Set()
     let uniqueList = new LinkedList()
     if(curr === null){
         return list.print()
     }else{
         while(curr.next){
            curr = curr.next
            values.add(curr.data)
         }
         [...values].forEach(el => uniqueList.add(el))
     }
     return uniqueList
 }

 console.log(removeDuplicates(ll).print())