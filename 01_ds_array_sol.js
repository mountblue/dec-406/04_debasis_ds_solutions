/**
 * Given an array A of size N, construct a Sum Array S(of same size) such that S is equal to the sum of all the elements of A except A[i]. Your task is to complete the function SumArray(A, N) which accepts the array A and N(size of array).
Input :
The first line of input is the number of testcases T. Each of the test case contains two lines. The first line of which is an integer N(size of array). The second line contains N space separated integers.
Output :
For each test case print the sum array in new line.
User Task:
Since this is a functional problem you did not have to worry about the input. You just have to complete the function SumArray().
Constraint :
1 <= T <= 10
1 <= N <= 104
1 <= Ai <= 104
Example:
Input:
2
5
3 6 4 8 9
6
4 5 7 3 10 1
Output:
27 24 26 22 21
26 25 23 27 20 29
Explanation:
Testcase 1: For the sum array S, at i=0 we have 6+4+8+9. At i=1, 3+4+8+9. At i=2, we have 3+6+8+9. At i=3, we have 3+6+4+9. At i = 4, we have 3+6+4+8. So S is 27 24 26 22 21.

 * @param {arr} arr 
 * @param {sizeOfAnArray} n 
 */

function sumArray(arr,n){
	const computeAll = []
    for(let i = 0; i <= n; i++){
		let value = arr.reduce((a,b) => a + b,0) - arr[i];
		computeAll.push(value)
	}
	return computeAll.filter(Boolean);
}
const sumAll = sumArray([3,6,4,8,9],5)
console.log(sumAll)

/**
 * This is a functional problem . Your task is to return the product of array elements under a given modulo.
The modulo operation finds the remainder after division of one number by another. For example, K(mod(m))=K%m= remainder obtained when K is divided by m.
Input:
The first line of input contains T denoting the number of testcases.Then each of the T lines contains a single positive integer N denotes number of element in array. Next line contain 'N' integer elements of the array.
Output:
Return the product of array elements under a given modulo.
That is, return (Array[0]Array[1]Array[2]...*Array[n])%modulo.
Constraints:
1<=T<=200
1<=N<=10^5
1<=ar[i]<=10^5
Example:
Input:
1
4
1 2 3 4
Output:
24
 */

function reducedBymodulo(arr,n){
	return arr.reduce((a,b) => a * b);
}
const value = reducedBymodulo([1,2,3,4],4)
console.log(value)

/**
 * Given an increasing sequence a[], we need to find the K-th missing contiguous element in the increasing sequence which is not present in the sequence. If no k-th missing element is there output -1.
Input:
The first line consists of an integer T i.e. the number of test cases. The first line of each test case consists of two integers N and K.Next line consists of N spaced integers.
Output:
For each test case, print the Kth missing number if present otherwise print -1.
Constraints:
1<=T<=100
1<=N,K,A[i]<=105
Examples:
Input
2
5 2
1 3 4 5 7
6 2
1 2 3 4 5 6
Output
6
-1
Explanation:
#TestCase 1:
K=2
We need to find the 2nd missing number in the array. Missing numbers are 2 and 6. So 2nd missing number is 6.
#Testcase 2:
K=2
We need to find the 2nd missing number in the array. As there is no missing number, hence the output is -1.
 */
function missingNumbers(n,k,arr){
	const missingValues  = []
	for(let i = arr[0]; i <= arr[arr.length-1];i++){
		if(!arr.includes(i)) missingValues.push(i)
	}
	return missingValues.length ? missingValues[k-1]:-1;
}

const misingNumber = missingNumbers(6,2,[1,2,3,4,5,6])
console.log(misingNumber)

/**
 * 4. Write a program to input a list of n integers in an array and arrange them in a way similar to the to-and-fro movement of a Pendulum.
The minimum element out of the list of integers, must come in center position of array. If there are even elements, then minimum element should be moved to (n-1)/2 index (considering that indexes start from 0)
The next number (next to minimum) in the ascending order, goes to the right, the next to next number goes to the left of minimum number and it continues like a Pendulum.
Input:
The first line of input contains an integer T denoting the number of test cases. Then T test cases follow. Each test case contains an integer n denoting the size of the array. Then next line contains N space separated integers forming the array.
Output:
Output the array in Pendulum Arrangement.
Constraints:
1<=T<=500
1<=N<=100
1<=a[i]<=1000
Example:
Input:
2
5
1 3 2 5 4
5
11 12 31 14 5
Output:
5 3 1 2 4
31 12 5 11 14
 */
function pendulom(arr){
	const sortedArray = arr.sort((a,b) => a - b)
	const middle = sortedArray.splice(0,1);
	let leftSide  = new Set(), rightSide = new Set();
	for(let i = 0; i < sortedArray.length-1 ; i+=2){
		for(let j = i + 1 ; j < sortedArray.length; j+=2){
			leftSide.add(sortedArray[i])
			rightSide.add(sortedArray[j])
		}
	}
	return [...[...rightSide].reverse(),...middle,...[...leftSide]]
}

const p = pendulom([1,2,3,4,5])
console.log(p)

/**
 * Given two arrays and a number x, find the pair whose sum is closest to x and the pair has an element from each array.
Input:
The first line consists of a single integer T, the number of test cases. For each test case, the first line contains 2 integers n & m denoting the size of two arrays. Next line contains n- space separated integers denoting the elements of array A and next lines contains m space separated integers denoting the elements of array B followed by an integer x denoting the number whose closest sum is to find.
Output:
For each test case, the output is 2 space separated integers whose sum is closest to x.
Constraints:
1<=T<=100
1<=n,m<=50
1<=A[i],B[i]<=500
Example:
Input:
2
4 4
1 4 5 7
10 20 30 40
32
4 4
1 4 5 7
10 20 30 40
50
Output:
1 30
7 40
Collapse 
 */

const reducer = (a,b) => a + b;

function closetSum(arr1,arr2,target){
	let possiblePairs = [];
	for(let i = 0; i <= arr1.length-1 ; i++){
		for(let j = 0; j <= arr2.length-1; j++){
			possiblePairs.push([arr1[i],arr2[j]])
		}
	}
	const sortedPosValues = possiblePairs.filter(arr => arr.reduce(reducer) <= target).sort((a,b) => a.reduce(reducer) - b.reduce(reducer))
	return sortedPosValues[sortedPosValues.length-1]
}
console.log(closetSum([1,2,3,4],[10,20,30,50],23))