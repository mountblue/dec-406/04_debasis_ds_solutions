const data = require("./dataStructures/binaryTree");
const { BinaryTree, Node } = data;

let tree1 = new BinaryTree()
const rootTest = tree1.root = new Node(1)
tree1.root.left = new Node(2)
tree1.root.right = new Node(3)
tree1.root.left.left = new Node(4)
tree1.root.left.right = new Node(5)
tree1.root.right.right = new Node(7)
tree1.root.left.left.left = new Node(8); 
tree1.root.right.left = new Node(9); 
const test = tree1.root.right.left.right = new Node(10);
// console.log(tree1.height(rootTest))

const tree3 = new BinaryTree()
tree3.root = new Node(4)
tree3.root.left = new Node(2)
tree3.root.right = new Node(9)
tree3.root.left.left = new Node(3)
tree3.root.left.right = new Node(5)
tree3.root.right.right = new Node(7)
// console.log(tree3)

/**
 *. Given a binary tree of size N and two nodes. Your task is to complete the function NumberOFTurn() that should return the count of the number of turns needs to reach from one node to another node of the Binary tree.
Example:
Input: Below Binary Tree and two nodes
5 & 6
				1
			/   \
			2    3
			/  \  /  \
		4   5  6   7
		/     / \
		8     9  10
Output: Number of Turns needed to reach
from 5 to 6: 3
Input: For above tree if two nodes are 1 & 4
Output: Straight line : 0 turn 
 */
// initialize the count at the global
 let count = 0;

/**
 * searchs the path for each given node
 * @param {*} node 
 * @param {*} data 
 * @param {*} track 
 */
function searchThePath(node, data, track = []){
	if(node === null){
		return false
	}
	track.push(node)
	if(node.data === data) {
		return true
	}

	if((node.left !== null &&  searchThePath(node.left, data, track) )||
	(node.right !== null && searchThePath(node.right, data, track))){
	   return true
   }
   track.pop()
   return false;
}

/**
 * Returns the lowest common ancesstors of nodes
 * @param {*} tree 
 * @param {*} data1 
 * @param {*} data2 
 * @param {*} cb 
 */

function lowestCommonAncesstors(tree,data1,data2,cb){
	let track1 = []
	let track2 = []
	if( !cb(tree.root,data1,track1) ||  !cb(tree.root,data2,track2)){
		return null;
	}else {
		cb(tree.root,data1,track1)
		cb(tree.root,data2,track2)
		const common = track1.filter( o => track2.some(({data}) => o.data === data))
		return common[common.length-1]
	}
}


/**
 * counts the turn to reach at each data 
 * @param {*} root 
 * @param {*} data 
 * @param {*} turn 
 */

function countTurns(root, data, turn) {
		if (root === null) 
            return false; 
  
        // if found the data value in tree 
        if (root.data === data) 
            return true; 
  
        // Case 1: 
        if (turn === true) { 
            if (countTurns(root.left, data, turn)) 
                return true; 
            if (countTurns(root.right, data, !turn)) { 
                count += 1; 
                return true; 
			} 
		// Case 2: 
		} else { 
            if (countTurns(root.right, data, turn)) 
                return true; 
            if (countTurns(root.left, data, !turn)) { 
                count += 1; 
                return true; 
            } 
        } 
        return false; 
}

/**
 * calculates the total number of turns to reach from one node to another
 * @param {*} root 
 * @param {*} first 
 * @param {*} second 
 */
function NumberOfTurn(root, first, second) { 
	let lcaNode = lowestCommonAncesstors(root, first, second, searchThePath); 

	// there is no path between these two node 
	if (lcaNode === null) 
		return -1; 
	count = 0; 

	// case 1: 
	if (lcaNode.data != first && lcaNode.data != second) { 

		// count number of turns needs to reached 
		// the second node from LCA 
		if (countTurns(lcaNode.right, second, false) 
				|| countTurns(lcaNode.left, second, true)) 
			; 

		// count number of turns needs to reached 
		// the first node from LCA 
		if (countTurns(lcaNode.left, first, true) 
				|| countTurns(lcaNode.right, first, false)) 
			; 
		return count + 1; 
	} 

	// case 2: 
	if (lcaNode.data === first) { 

		// count number of turns needs to reached 
		// the second node from LCA 
		countTurns(lcaNode.right, second, false); 
		countTurns(lcaNode.left, second, true); 
		return count; 
	} else { 

		// count number of turns needs to reached 
		// the first node from LCA1 
		countTurns(lcaNode.right, first, false); 
		countTurns(lcaNode.left, first, true); 
		return count; 
	} 
} 

console.log(NumberOfTurn(tree1, 2,3))


/**
 * Given a binary tree of size N+1, your task is to complete the function tiltTree(), that return the tilt of the whole tree. The tilt of a tree node is defined as the absolute difference between the sum of all left subtree node values and the sum of all right subtree node values. Null nodes are assigned tilt to be zero. Therefore, tilt of the whole tree is defined as the sum of all nodes’ tilt.
Examples:
Input :
1
/ \
2 3
Output : 1
Explanation:
Tilt of node 2 : 0
Tilt of node 3 : 0
Tilt of node 1 : |2-3| = 1
Tilt of binary tree : 0 + 0 + 1 = 1
Input :
	 4
   /  \
  2   9
 / \   \
3  5   7
Output : 15
Explanation:
Tilt of node 3 : 0
Tilt of node 5 : 0
Tilt of node 7 : 0
Tilt of node 2 : |3-5| = 2
Tilt of node 9 : |0-7| = 7
Tilt of node 4 : |(3+5+2)-(9+7)| = 6
Tilt of binary tree : 0 + 0 + 0 + 2 + 7 + 6 = 15
 
 * @param {*} node 
 * @param {*} sum 
 */


function calculateTilt(node){
	if(node === null) {
		return 0
	}
	if(node.left && node.right){
		return Math.abs(node.left.data - node.right.data) + calculateTilt(node.left) + calculateTilt(node.right)
	}else if(node.left && !node.right){
		return node.left.data + calculateTilt(node.left)
	}else if(node.right && !node.left) {
		return node.right.data + calculateTilt(node.right)
	}else {
		return 0
	}
}
console.log(calculateTilt(tree3.root))