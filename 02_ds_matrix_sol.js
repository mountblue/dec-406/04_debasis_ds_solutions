/**
 * There is the Rectangular path for a Train to travel consisting of n and m rows and columns respectively. The train will start from one of grid cells and it will be given a command in the form of String s. consisting of characters ‘L’, ‘R’, ‘U’, ‘D’.The train will follow the instructions of the command string, where 'L' corresponds moving to the left, 'R' towards the right, 'U' for moving up, and 'D' means down.
You have already selected the command string s, and are wondering if it is possible to place the train in one of the grid cells initially and have it always stay entirely within the grid upon execution of the command string s. Output “1” if there is a starting cell for which the train doesn’t fall off the grid(track) on following command s, otherwise, output "0".
Input:
The first line of input will contain an integer T, the number of test cases.
Each test case will be on two lines.
The first line will have two space separated integers n,m.
The second line will have the command string s.
Output:
For each test case, output "1" (without quotes) or "0" (without quotes) in a new line.
Constraints:
1 <= T <= 1,00
1 <= n,m <= 10
1 <= |s| <= 10
Example:
Input:
2
1 1
R
2 3
LLRU
Output:
0
1
 * @param {*} coordinates 
 * @param {*} commands 
 */

function mapCoordinates(coordinates, commands){
    let [x, y] = coordinates;
    [...commands].forEach(c => {
        if( c  === "L"){
            y -= 1;
        }
        if( c === "R") {
            y += 1;
        }
        if( c === "U"){
            x -= 1;
        }
        if( c === "D"){
            x += 1;
        }
    })
    return [x,y]
}


function matrixPositions(matrix, commands){
    let eachCoordinates = []
    let cube;
    for(let i = 0; i < matrix.length; i++) {
        cube = matrix[i];
        for(let j = 0; j < cube.length; j++) {
           eachCoordinates.push([i,j])
        }
    }
    
    eachCoordinates = eachCoordinates.map(coords => {
        return mapCoordinates(coords, commands)
    }).filter(el => (el[0] >= 0 && el[0] < matrix.length ) && (el[1] >= 0 && el[1] < cube.length))
    return eachCoordinates.length ? 1 : 0;
}
// console.log(matrixPositions(matrix,"LLRU"))


/**
 * Given an incomplete Sudoku configuration in terms of a 9x9 2-D square matrix (mat[][]) the task to check if the configuration has a solution or not.
Input:
The first line of input contains an integer T denoting the no of test cases. Then T test cases follow. Each test case contains 9*9 space separated values of the matrix mat[][] representing an incomplete Sudoku state where a 0 represents empty block.
Output:
For each test case in a new line print 1 if the sudoku configuration is valid else print 0.
Constraints:
1<=T<=10
0<=mat[]<=9
Example:
Input:
2
3 0 6 5 0 8 4 0 0 5 2 0 0 0 0 0 0 0 0 8 7 0 0 0 0 3 1 0 0 3 0 1 0 0 8 0 9 0 0 8 6 3 0 0 5 0 5 0 0 9 0 6 0 0 1 3 0 0 0 0 2 5 0 0 0 0 0 0 0 0 7 4 0 0 5 2 0 6 3 0 0
3 6 7 5 3 5 6 2 9 1 2 7 0 9 3 6 0 6 2 6 1 8 7 9 2 0 2 3 7 5 9 2 2 8 9 7 3 6 1 2 9 3 1 9 4 7 8 4 5 0 3 6 1 0 6 3 2 0 6 1 5 5 4 7 6 5 6 9 3 7 4 5 2 5 4 7 4 4 3 0 7
Output:
1
0
 */

function sudokuSolution(matrix){
    let compulsoryValues = [1,2,3,4,5,6,7,8,9]
    let flag = false;
    matrix.every((el,i) => {
        if(el.length === new Set(el).length){
            flag = true     
        }else{
            let fullPlaces = el.filter(Boolean)
            let emptyPlaces = el.filter(value => Boolean(value) === false);
            let missingElements = compulsoryValues.filter(val => !fullPlaces.includes(val))
            if(missingElements.length !==  emptyPlaces.length){
                flag = false
            }else{
                flag = true
            }
        }

    })
    return flag ? 1 : 0;
}

// console.log(sudokuSolution(val))

/**
 * 3. Given a N X N matrix (M) filled with 1 , 0 , 2 , 3 . Your task is to find whether there is a path possible from source to destination, while traversing through blank cells only. You can traverse up, down, right and left.
A value of cell 1 means Source.
A value of cell 2 means Destination.
A value of cell 3 means Blank cell.
A value of cell 0 means Blank Wall.
Note : there is only single source and single destination.
Examples:
Input : M[3][3] = {{ 0 , 3 , 2 },
{ 3 , 3 , 0 },
				{ 1 , 3 , 0 }};
Output : Yes
Input : M[4][4] = {{ 0 , 3 , 1 , 0 },
{ 3 , 0 , 3 , 3 },
				{ 2 , 3 , 0 , 3 },
				{ 0 , 3 , 3 , 3 }};
Output : Yes
Input:
The first line of input is an integer T denoting the no of test cases. Then T test cases follow. Each test case consists of 2 lines . The first line of each test case contains an integer N denoting the size of the square matrix . Then in the next line are N*N space separated values of the matrix (M) .
Output:
For each test case in a new line print 1 if the path exist from source to destination else print 0.
Constraints:
1<=T<=20
1<=N<=20
Example:
Input:
2
4
3 0 0 0 0 3 3 0 0 1 0 3 0 2 3 3
3
0 3 2 3 0 0 1 0 0
Output:
1
0
 */
// solution:

/**
 * 4. Given an square matrix, turn it by 90 degrees in anti-clockwise direction without using any extra space.
Input:
The first line of input contains a single integer T denoting the number of test cases. Then T test cases follow. Each test case consist of two lines. The first line of each test case consists of an integer N, where N is the size of the square matrix.The second line of each test case contains NxN space separated values of the matrix M.
Output:
Corresponding to each test case, in a new line, print the rotated array.
Constraints:
1 ≤ T ≤ 50
1 ≤ N ≤ 50
Example:
Input
1
3
1 2 3 4 5 6 7 8 9
Output
3 6 9 2 5 8 1 4 7
 */

function rotateTheMatrix(matrix){
    let reversed = []
    let length;
    let transposed = []
    for(let i = 0; i <= matrix.length-1; i++) {
        cube = matrix[i];
        length = cube.length;
        for(let j = 0; j <= cube.length-1 ;j++) {
            reversed.push(matrix[j][i])
        }
    }
    while(reversed.length){
        transposed.unshift(reversed.splice(0,length))
    }
    return transposed
}

// const rotated = rotateTheMatrix(
//     [[1,2,3],
//     [4,5,6],
//     [7,8,9],
// ]
//     );
// console.log(rotated)

/**
 * 5. Given a 2D matrix of size M*N. Traverse and print the matrix in spiral form.
Input:
The first line of the input contains a single integer T, denoting the number of test cases. Then T test cases follow. Each testcase has 2 lines. First line contains M and N respectively separated by a space. Second line contains M*N values separated by spaces.
Output:
Elements when travelled in Spiral form, will be displayed in a single line.
Constraints:
1 <=T<= 100
2 <= M, N <= 10
0 <= Ai <= 100
Example:
Input:
1
4 4
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16
Output:
1 2 3 4 8 12 16 15 14 13 9 5 6 7 11
 */

function traverseAndPrintSpiral(matrix){
    matrix.forEach((el,i) => {
        if((i+1) % 2 === 0){
            el.reverse()
        }
        el.forEach((val) => {
            console.log(val)
        })
    })
    
}

traverseAndPrintSpiral([[1,2,3,4],
    [5,6,7,8],
    [9,10,11,12],
[13,14,15,16]])